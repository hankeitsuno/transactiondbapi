﻿using Microsoft.AspNetCore.Mvc;
using Serilog;
using TransactionDb.Application;
using TransactionDb.Model;

namespace TransactionDb.API.Controller
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionDbContoller : ControllerBase
    {
        private ITransactionDbReceiver _receiver;

        public TransactionDbContoller(ITransactionDbReceiver receiver)
        {
            this._receiver = receiver;
        }

        [HttpPut("SendQueryTransaction")]
        public ProcessResult PutSendQueryTransactionDb([FromQuery]FlowIntegrationDataStruct integrationDataStruct)
        {
            ProcessResult result = _receiver.SendQueryTransactionDb(integrationDataStruct);

            Log.Information("Rota OUT: " + result);

            return result;
        }
    }
}