using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Core;
using System;
using System.Threading.Tasks;
using ILogger = Serilog.ILogger;

namespace TransactionDb.API
{
    public class Program
    {
        public static readonly string Namespace = typeof(Program).Namespace;

        public static readonly string AppName = Namespace.Substring(Namespace.LastIndexOf('.', Namespace.LastIndexOf('.') - 1) + 1);

        public static async Task Main(string[] args)
        {
            var configuration = GetConfiguration();

            Log.Logger = CreateSerilogLogger(configuration);

            var webHost = CreateHostBuilder(configuration, args).Build();

            await webHost.RunAsync();
        }

        public static IHostBuilder CreateHostBuilder(IConfiguration configuration, string[] args) =>
         Host.CreateDefaultBuilder(args)
             .ConfigureWebHostDefaults(webBuilder =>
             {
                 webBuilder.UseStartup<Startup>();
                 webBuilder.UseConfiguration(configuration);
             })
            .ConfigureWebHost(config =>
            {
                var port = configuration.GetValue<string>("PortNumber");
                config.UseUrls($"http://*:{port}");
            })
            .UseWindowsService();

        private static ILogger CreateSerilogLogger(IConfiguration configuration)
        {
            LoggingLevelSwitch levelSwitch = new LoggingLevelSwitch();

            switch (configuration["Serilog:MinimumLevel"])
            {
                case "Verbose": levelSwitch.MinimumLevel = Serilog.Events.LogEventLevel.Verbose; break;
                case "Debug": levelSwitch.MinimumLevel = Serilog.Events.LogEventLevel.Debug; break;
                case "Information": levelSwitch.MinimumLevel = Serilog.Events.LogEventLevel.Information; break;
                case "Warning": levelSwitch.MinimumLevel = Serilog.Events.LogEventLevel.Warning; break;
                case "Error": levelSwitch.MinimumLevel = Serilog.Events.LogEventLevel.Error; break;
                case "Fatal": levelSwitch.MinimumLevel = Serilog.Events.LogEventLevel.Fatal; break;
                default: levelSwitch.MinimumLevel = Serilog.Events.LogEventLevel.Warning; break;
            }

            return new LoggerConfiguration()
                .MinimumLevel.ControlledBy(levelSwitch)
                .Enrich.WithProperty("ApplicationContext", AppName)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .WriteTo.File(configuration["Serilog:PathLog"] + "\\.txt", rollingInterval: RollingInterval.Hour)
                .ReadFrom.Configuration(configuration)
                .CreateLogger();
        }

        private static IConfiguration GetConfiguration()
        {
            var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            var appSettings = $"appsettings{(!string.IsNullOrEmpty(environment) ? $".{environment}" : string.Empty)}.json";

            var builder = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile(appSettings, optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            var configuration = builder.Build();

            return configuration;
        }
    }
}
