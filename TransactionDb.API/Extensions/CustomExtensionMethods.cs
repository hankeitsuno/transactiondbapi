﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;

namespace TransactionDb.API.Extensions
{
    public static class CustomExtensionMethods
    {
        public static IServiceCollection AddLoggerFactory(this IServiceCollection services)
        {
            services.AddSingleton<ILoggerFactory>(services => new Serilog.Extensions.Logging.SerilogLoggerFactory(Log.Logger));
            return services;
        }

        public static IServiceCollection AddCustomSwagger(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSwaggerGen(options =>
            {
                //options.DescribeAllEnumsAsStrings();
                options.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Title = "TransactionDb HTTP API",
                    Version = "v1",
                    Description = "TransactionDb HTTP API"
                });
            });

            return services;
        }


        public static IServiceCollection AddCustomMvc(this IServiceCollection services)
        {
            services
                    .AddMvc(config => config.Filters.Add(new ExceptionHandlingAttribute()))
                    .AddControllersAsServices();

            return services;
        }

        public class ExceptionHandlingAttribute : ExceptionFilterAttribute
        {
            public override void OnException(ExceptionContext context)
            {
                base.OnException(context);

                Log.Logger.Error(context.Exception, "");
            }
        }
    }
}
