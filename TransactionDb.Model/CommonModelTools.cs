﻿using System.Data.SqlClient;

namespace TransactionDb.Model
{
    public class CommonModelTools
    {
        public static string BuildStringList(string[] arrayString)
        {
            string condition = string.Empty;

            foreach (var _string in arrayString)
                condition += string.Concat("'", _string, "',");

            condition = condition[0..^1];
            return condition;
        }

        public static SqlConnection GetSqlConnection(string connectionString,string connectionStringSec)
        {
            try
            {
                return new SqlConnection(connectionString);

            }
            catch (System.Exception)
            {
                return new SqlConnection(connectionStringSec);

                throw;
            }
        }
       
    }
}
