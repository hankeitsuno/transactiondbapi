﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TransactionDb.Model
{
    public enum ScriptAction
    { 
        Consulta,
        Manipulacao    
    }
    public class FlowIntegrationDataStruct
    {
        [Required]
        public int IdCustomer { get; set; }
        [Required]
        public string ScriptTransaction { get; set; } = string.Empty;
        [Required]
        public ScriptAction Action { get; set; }
        public DateTime TransactionDate { get; set; }
        [Required]
        public int IdDevice { get; set; }

        public bool localDb { get; set; } = false;

    }   
}
