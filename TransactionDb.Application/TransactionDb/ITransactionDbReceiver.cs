﻿
using TransactionDb.Model;

namespace TransactionDb.Application
{
    public interface ITransactionDbReceiver
    {
        ProcessResult SendQueryTransactionDb(FlowIntegrationDataStruct integrationDataStruct);
    }
}
