﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using TransactionDb.Infrastructure;
using TransactionDb.Model;

namespace TransactionDb.Application
{
    public class TransactionDbReceiver : ITransactionDbReceiver
    {
        public ITransactionDbRepository _repository;
        public string dbConnectionStringClient { get; set; }

        private string currentConnectionString { get; set; }

        private IConfiguration _configuration;
        public TransactionDbReceiver(ITransactionDbRepository repository
                            , IConfiguration configuration)
        {
            _repository = repository;
            _configuration = configuration;
        }


        public ProcessResult SendQueryTransactionDb(FlowIntegrationDataStruct integrationDataStruct)
        {
            Stopwatch stopwatch = new Stopwatch();
            ProcessResult result = null;
            SqlConnection connectionClient = new SqlConnection();

            currentConnectionString = _configuration["StringConnectionDbMaster"];

            string messageResult = string.Empty;

            int tryquantity = 0;

            while (tryquantity < 10)
            {
                stopwatch.Start();

                SqlTransaction transaction = null;

                using (SqlConnection connection = CommonModelTools.GetSqlConnection(currentConnectionString, _configuration["StringConnectionDbSlave"]))
                {
                    try
                    {
                        connection.Open();

                        if (!ValidateContentFlowIntegration(integrationDataStruct, connection, out messageResult))
                            return new ProcessResult() { Message = "Revise os dados enviados", Sucess = false, Content = messageResult };

                        if (!integrationDataStruct.localDb)
                            connectionClient.ConnectionString = dbConnectionStringClient;
                        else
                            connectionClient.ConnectionString = currentConnectionString;

                        connectionClient.Open();

                        transaction = connectionClient.BeginTransaction();

                        if (integrationDataStruct.Action == ScriptAction.Consulta)
                        {
                            DataTable resultConsultTransaction = _repository.GetQueryByTransaction(connectionClient, transaction, integrationDataStruct.ScriptTransaction);
                            result = new ProcessResult() { Message = "Transação efetuada com sucesso ", Sucess = true, Content = resultConsultTransaction };
                        }
                        else
                        {
                            _repository.ExecManipulationByTransaction(connectionClient, transaction, integrationDataStruct.ScriptTransaction);
                            result = new ProcessResult() { Message = "Transação efetuada com sucesso ", Sucess = true, Content = "Transação de manipulação finalizada com sucesso" };
                        }

                        transaction.Commit();

                        _repository.InsertTranscationsHistoryDb(connection
                                                              , integrationDataStruct.IdCustomer
                                                              , integrationDataStruct.Action.ToString()
                                                              , integrationDataStruct.ScriptTransaction
                                                              , integrationDataStruct.TransactionDate
                                                              , integrationDataStruct.IdDevice
                                                              , stopwatch.ElapsedMilliseconds);

                        stopwatch.Stop();

                        return result;
                    }

                    catch (Exception ex)
                    {
                        if (transaction != null && transaction.Connection != null && connectionClient.State == System.Data.ConnectionState.Open)
                            transaction.Rollback();

                        stopwatch.Stop();

                        if (connectionClient.State != System.Data.ConnectionState.Open)
                        {
                            Log.Information("Conexão com banco principal perdida, iniciando conexão com banco local");

                            if (currentConnectionString == _configuration["StringConnectionDbMaster"])
                                currentConnectionString = _configuration["StringConnectionDbSlave"];
                            else
                                currentConnectionString = _configuration["StringConnectionDbMaster"];

                            Task.Delay(200);
                            tryquantity++;
                        }
                        else
                        {
                            Log.Error("Erro ao executar a transação: " + ex.ToString());
                            return new ProcessResult() { Message = ex.ToString(), Sucess = false };
                        }
                    }
                }
            }

            return new ProcessResult() { Message = "Tentativas excedidas, não foi possivel executar o comando", Sucess = false };
        }


        private bool ValidateContentFlowIntegration(FlowIntegrationDataStruct integrationDataStruct, SqlConnection connection, out string message)
        {
            bool validated = true;
            string messageResult = string.Empty;

            if (_repository.ValidateStructCustomerDevice(connection, integrationDataStruct.IdCustomer, integrationDataStruct.IdDevice) == 0)
            {
                validated = false;
                messageResult += $"Parametro: IdCustomer : {integrationDataStruct.IdCustomer} ou  IdDevice : {integrationDataStruct.IdDevice} +  não cadastrado";
            }
            else
                dbConnectionStringClient = _repository.GetConnectionStringClientDb(connection, integrationDataStruct.IdCustomer);


            if (integrationDataStruct.TransactionDate == DateTime.MinValue)
                integrationDataStruct.TransactionDate = DateTime.Now;

            message = messageResult;
            return validated;
        }

    }
}
