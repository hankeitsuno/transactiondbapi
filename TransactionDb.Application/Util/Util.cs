﻿using Sequor.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sequor.Application
{
    public class Util
    {
        public static List<SqoLineStation> FakeGetPlacesListPersistence = new List<SqoLineStation>
        {
            new SqoLineStation
            {
                IdPlace = 1,
                Place = "LINHA01",
                DescriptionStation = "Linha 1",
                AmountUsersAllowed = 2,
                Places = new List<SqoLineStation>
                {
                    new SqoLineStation
                    {
                        IdPlace = 3,
                        Place = "EST01",
                        DescriptionStation = "Estação 1",
                        AmountUsersAllowed = 2,
                        SkillsPlace = new List<SqoSkillsLineStation>
                        {
                            new SqoSkillsLineStation
                            {
                                Id = 1,
                                Skill = "Habilidade01"
                            },
                            new SqoSkillsLineStation
                            {
                                Id = 2,
                                Skill = "Habilidade02"
                            }
                        }
                    },
                    new SqoLineStation
                    {
                        IdPlace = 4,
                        Place = "EST02",
                        DescriptionStation = "Estação 2",
                        AmountUsersAllowed = 1,
                        SkillsPlace = new List<SqoSkillsLineStation>
                        {
                            new SqoSkillsLineStation
                            {
                                Id = 2,
                                Skill = "Habilidade02"
                            },
                            new SqoSkillsLineStation
                            {
                                Id = 3,
                                Skill = "Habilidade03"
                            }
                        }
                    },
                    new SqoLineStation
                    {
                        IdPlace = 5,
                        Place = "EST03",
                        DescriptionStation = "Estação 3",
                        AmountUsersAllowed = 2,
                        SkillsPlace = new List<SqoSkillsLineStation>
                        {
                            new SqoSkillsLineStation
                            {
                                Id = 3,
                                Skill = "Habilidade03"
                            },
                            new SqoSkillsLineStation
                            {
                                Id = 4,
                                Skill = "Habilidade04"
                            }
                        }
                    },
                }
            },
            new SqoLineStation
            {
                IdPlace = 2,
                Place = "LINHA02",
                DescriptionStation = "Linha 2",
                AmountUsersAllowed = 2,
                Places = new List<SqoLineStation>
                {
                    new SqoLineStation
                    {
                        IdPlace = 6,
                        Place = "EST01",
                        DescriptionStation = "Estação 1",
                        AmountUsersAllowed = 2,
                        SkillsPlace = new List<SqoSkillsLineStation>
                        {
                            new SqoSkillsLineStation
                            {
                                Id = 1,
                                Skill = "Habilidade01"
                            },
                            new SqoSkillsLineStation
                            {
                                Id = 2,
                                Skill = "Habilidade02"
                            }
                        }
                    },
                    new SqoLineStation
                    {
                        IdPlace = 7,
                        Place = "EST02",
                        DescriptionStation = "Estação 2",
                        AmountUsersAllowed = 1,
                        SkillsPlace = new List<SqoSkillsLineStation>
                        {
                            new SqoSkillsLineStation
                            {
                                Id = 2,
                                Skill = "Habilidade02"
                            },
                            new SqoSkillsLineStation
                            {
                                Id = 3,
                                Skill = "Habilidade03"
                            }
                        }
                    },
                    new SqoLineStation
                    {
                        IdPlace = 8,
                        Place = "EST03",
                        DescriptionStation = "Estação 3",
                        AmountUsersAllowed = 2,
                        SkillsPlace = new List<SqoSkillsLineStation>
                        {
                            new SqoSkillsLineStation
                            {
                                Id = 3,
                                Skill = "Habilidade03"
                            },
                            new SqoSkillsLineStation
                            {
                                Id = 4,
                                Skill = "Habilidade04"
                            }
                        }
                    },
                    new SqoLineStation
                        {
                            IdPlace = 9,
                            Place = "EST04",
                            DescriptionStation = "Estação 4",
                            AmountUsersAllowed = 3,
                            SkillsPlace = new List<SqoSkillsLineStation>
                            {
                                new SqoSkillsLineStation
                                {
                                    Id = 4,
                                    Skill = "Habilidade04"
                                },
                                new SqoSkillsLineStation
                                {
                                    Id = 5,
                                    Skill = "Habilidade05"
                                }
                            }
                        },
                    new SqoLineStation
                        {
                            IdPlace = 10,
                            Place = "EST05",
                            DescriptionStation = "Estação 5",
                            AmountUsersAllowed = 3,
                            SkillsPlace = new List<SqoSkillsLineStation>
                            {
                                new SqoSkillsLineStation
                                {
                                    Id = 5,
                                    Skill = "Habilidade05"
                                },
                                new SqoSkillsLineStation
                                {
                                    Id = 6,
                                    Skill = "Habilidade06"
                                }
                            }
                        }
                }
            }
        };
    }
}
