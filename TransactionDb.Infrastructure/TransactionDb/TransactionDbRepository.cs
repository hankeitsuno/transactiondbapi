﻿using Sequor.DB.Persistencia;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace TransactionDb.Infrastructure
{
    public class TransactionDbRepository : ITransactionDbRepository
    {

        public void InsertTranscationsHistoryDb(SqlConnection connection
                                                , int IdCustomer
                                                , string Action
                                                , string ScriptTransaction
                                                , DateTime TransactionDate
                                                , int IdDevice
                                                , long ProcessTime)
        {
            string query = string.Empty;

            query = @"
                        Insert 
                        	FlowIntegrationData
                        	   (
                        		 IdCustomer
                        		,[Action]
                        		,ScriptTransaction
                        		,TransactionDate
                        		,IdDevice
                        		,ProcessTime   
                        	   )
                        Values
                        	   (	   
                        		 @IdCustomer
                        		,@Action
                        		,@ScriptTransaction
                        		,@TransactionDate
                        		,@IdDevice
                        		,@ProcessTime 
                        	   )          
                    ";

            using (SqlCommand command = new SqlCommand(query, connection))
            {
                command.Parameters.AddWithValue("@IdCustomer", IdCustomer);
                command.Parameters.AddWithValue("@Action", Action);
                command.Parameters.AddWithValue("@ScriptTransaction", ScriptTransaction);
                command.Parameters.AddWithValue("@TransactionDate", TransactionDate);
                command.Parameters.AddWithValue("@IdDevice", IdDevice);
                command.Parameters.AddWithValue("@ProcessTime", ProcessTime);

                command.ExecuteNonQuery();
            }
        }

        public DataTable GetQueryByTransaction(SqlConnection connection, SqlTransaction transaction, string transactionQuery)
        {
            string query = transactionQuery;

            DataTable dataTable = new DataTable();

            using (var reader = Persistencia.ExecuteReader(connection, query, transaction))
            {
                while (reader.Read())
                {
                    bool createColumns = dataTable.Columns.Count == 0;

                    DataRow dataRow = dataTable.NewRow();

                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        if (createColumns)
                            dataTable.Columns.Add(reader.GetName(i));

                        dataRow[reader.GetName(i)] = reader[i] != DBNull.Value ? reader[i] : default;
                    }

                    dataTable.Rows.Add(dataRow);
                }
            }
            return dataTable;
        }

        public void ExecManipulationByTransaction(SqlConnection connection, SqlTransaction transaction,string transactionManipulation)
        {
            string query = transactionManipulation;

            Persistencia.ExecuteNonQuery(connection, query, transaction);
        }

        public int ValidateStructCustomerDevice(SqlConnection connection, int idCustomer, int idDevice) 
        {
            string query = @"
                            Select
                            	Custm.Id
                            From
                            	CustomerConfigIntegrationData as Custm With (Nolock)
                            Inner Join
                            	CustomerDevicesIntegrationData as Dev With (Nolock)
                            On
                            	Custm.Id = Dev.IdCustomer
							Where
								Custm.Id = @IdCustomer
							And
								Dev.ID = @IdDevice";
            
            return Convert.ToInt32(Persistencia.ExecuteScalar(connection, query, new SqlParameter("@IdCustomer", idCustomer)
                                                                               , new SqlParameter("@IdDevice", idDevice)));
        }

        public string GetConnectionStringClientDb(SqlConnection connection, int idCustomer)
        {
            string query = @"
                           
                Select 
                    StringConnection 
                From
                    CustomerConfigIntegrationData
                Where 
                    Id = @IdCustomer";

            return Persistencia.ExecuteScalar(connection, query, new SqlParameter("@IdCustomer", idCustomer)).ToString();
        }

    }
}

