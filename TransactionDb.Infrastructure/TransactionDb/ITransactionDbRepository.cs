﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace TransactionDb.Infrastructure
{
    public interface ITransactionDbRepository
    {
        void InsertTranscationsHistoryDb(SqlConnection connection
                                                , int IdCustomer
                                                , string Action
                                                , string ScriptTransaction
                                                , DateTime TransactionDate
                                                , int IdDevice
                                                , long ProcessTime);

        DataTable GetQueryByTransaction(SqlConnection connection, SqlTransaction transaction, string transactionQuery);
        void ExecManipulationByTransaction(SqlConnection connection, SqlTransaction transaction, string transactionManipulation);

        int ValidateStructCustomerDevice(SqlConnection connection, int idCustomer,int idDevice);

        string GetConnectionStringClientDb(SqlConnection connection, int idCustomer);
    }
}
