FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443
RUN sed -i 's/DEFAULT@SECLEVEL=2/DEFAULT@SECLEVEL=1/g' /etc/ssl/openssl.cnf

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY ["Sequor.NotificationCenter.API/Sequor.NotificationCenter.API.csproj", "Sequor.NotificationCenter.API/"]
RUN dotnet restore "Sequor.NotificationCenter.API/Sequor.NotificationCenter.API.csproj"
COPY . .
WORKDIR /src/Sequor.NotificationCenter.API
RUN dotnet build "Sequor.NotificationCenter.API.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "Sequor.NotificationCenter.API.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "Sequor.NotificationCenter.API.dll"]